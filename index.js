const onError = require('./onError')

module.exports = (opts) => {
  return ({
    onError: (handler, next) => onError(handler, next, opts)
  })
}
