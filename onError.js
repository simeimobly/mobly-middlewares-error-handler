module.exports = (handler, next, opts = {}) => {
  const defaults = {
    customHttpErrors: { InternalServerError: { statusCode: 500, body: { message: 'Internal Server Error' } } },
    customLogger: () => console.error
  }

  // sobrescrevendo defaults com configurações customizadas
  const options = {}
  options.customLogger = Object.assign(defaults.customLogger, opts.customLogger)
  options.customHttpErrors = Object.assign(defaults.customHttpErrors, opts.customHttpErrors)
  options.nonHttpErrors = opts.nonHttpErrors

  // verifica se é erro http
  handler.response = (handler.error.statusCode && handler.error.message) ? isCustomError(handler.error, options.customHttpErrors) : nonHTTPError(handler.error, options)

  // logger
  if (typeof options.customLogger === 'function') options.customLogger(handler.error)

  // trata body
  handler.response.body = (typeof handler.response.body !== 'string') ? JSON.stringify(handler.response.body) : handler.response.body

  return next()
}

// verifica se tem erros customizados
const isCustomError = (err, customHttpErrors) => {
  return (customHttpErrors[err.name]) ? isCustomProcess(err, customHttpErrors[err.name]) : isStandardError(err, customHttpErrors)
}

// verifica se tem processamento de erro customizado
const isCustomProcess = (err, customError) => {
  return (customError.process) ? customError.process(err) : customError
}

// monta erro http padrão
const isStandardError = (err, customHttpErrors) => {
  const statusCode = err.status || customHttpErrors.status
  let body = (err.message) ? { message: err.message } : err.body
  body = body || customHttpErrors.body
  return { statusCode, body }
}

// veririca se tem erros não http customizados
const nonHTTPError = (err, options) => {
  return (typeof options.nonHttpErrors === 'function') ? options.nonHttpErrors(err) : genericError(options.customHttpErrors)
}

// monta erro genérico para erro não tratado
const genericError = (customHttpErrors) => customHttpErrors.InternalServerError
