const onError = require('./onError')
const createError = require('http-errors')

const notFoundError = createError.NotFound()

describe('Error Handler', () => {
  it('success - standard http error', async (done) => {
    const handler = { error: notFoundError }

    onError(handler, () => {
      const body = JSON.parse(handler.response.body)
      expect(handler.response.statusCode).toEqual(404)
      expect(body.message).toEqual('Not Found')
      done()
    }, {})
  })

  it('success - custom http error', async (done) => {
    const handler = { error: notFoundError }
    const customHttpErrors = {
      NotFoundError: {
        statusCode: 404,
        body: { message: 'Digital object not found' }
      }
    }

    onError(handler, () => {
      const body = JSON.parse(handler.response.body)
      expect(handler.response.statusCode).toEqual(404)
      expect(body.message).toEqual('Digital object not found')
      done()
    }, { customHttpErrors })
  })

  it('success - custom processed http error', async (done) => {
    const handler = { error: notFoundError }
    const customHttpErrors = {
      NotFoundError: {
        process: (err) => {
          return {
            statusCode: 404,
            body: { message: err.message.replace(/ /gi, '_') }
          }
        }
      }
    }

    onError(handler, () => {
      const body = JSON.parse(handler.response.body)
      expect(handler.response.statusCode).toEqual(404)
      expect(body.message).toEqual('Not_Found')
      done()
    }, { customHttpErrors })
  })

  it('success - generic error', async (done) => {
    const genericError = new Error('Unexpected unidentified error')
    const handler = { error: genericError }

    onError(handler, () => {
      const body = JSON.parse(handler.response.body)
      expect(handler.response.statusCode).toEqual(500)
      expect(body.message).toEqual('Internal Server Error')
      done()
    }, {})
  })

  it('success - non http error', async (done) => {
    const genericError = new Error('Unexpected unidentified error')
    const handler = { error: genericError }
    const nonHttpErrors = () => {
      return { statusCode: 500, body: { message: 'Something went very very wrong' } }
    }

    onError(handler, () => {
      const body = JSON.parse(handler.response.body)
      expect(handler.response.statusCode).toEqual(500)
      expect(body.message).toEqual('Something went very very wrong')
      done()
    }, { nonHttpErrors })
  })
})
